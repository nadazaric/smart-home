package nvt.project.smart_home.main.feature.device.ambient_sensor.web_dto.response;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import nvt.project.smart_home.main.core.dto.response.SmartDeviceResponseDto;

@Getter
@Setter
@SuperBuilder
public class AmbientSensorWebResponseDto extends SmartDeviceResponseDto {
}
