package nvt.project.smart_home.main.core.constant;

public enum Role {
    SUPER_ADMIN,
    ADMIN,
    USER
}
