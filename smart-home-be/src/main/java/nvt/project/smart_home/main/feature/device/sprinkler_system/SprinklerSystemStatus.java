package nvt.project.smart_home.main.feature.device.sprinkler_system;

public enum SprinklerSystemStatus {
    ON,
    OFF
}
