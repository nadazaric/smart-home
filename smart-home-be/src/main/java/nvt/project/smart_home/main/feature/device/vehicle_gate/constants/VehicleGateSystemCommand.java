package nvt.project.smart_home.main.feature.device.vehicle_gate.constants;

public enum VehicleGateSystemCommand {
    IN,
    OUT,
    DENIED,
    NO_CHANGES,
    CLOSE,
    USER_CHANGE
}
