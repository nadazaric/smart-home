package nvt.project.smart_home.main.core.dto.response;

import lombok.*;

@Getter
@Setter
@Builder
public class UserRefResponseDto {

    private Long id;
    private String email;

}
