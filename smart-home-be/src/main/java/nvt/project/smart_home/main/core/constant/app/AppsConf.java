package nvt.project.smart_home.main.core.constant.app;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AppsConf {
    public static final boolean SCRIPT_APP = false;
}
