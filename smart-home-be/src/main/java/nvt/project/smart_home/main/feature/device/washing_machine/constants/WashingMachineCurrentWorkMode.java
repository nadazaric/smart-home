package nvt.project.smart_home.main.feature.device.washing_machine.constants;

public enum WashingMachineCurrentWorkMode {
    STANDARD_WASH_PROGRAM,
    COLOR_WASH_PROGRAM,
    WASH_PROGRAM_FOR_DELICATES,
    SCHEDULED_STANDARD_WASH_PROGRAM,
    SCHEDULED_COLOR_WASH_PROGRAM,
    SCHEDULED_WASH_PROGRAM_FOR_DELICATES,
    OFF,

}
