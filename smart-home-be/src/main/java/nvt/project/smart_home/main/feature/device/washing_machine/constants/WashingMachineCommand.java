package nvt.project.smart_home.main.feature.device.washing_machine.constants;

public enum WashingMachineCommand {
    STANDARD_WASH_PROGRAM,
    COLOR_WASH_PROGRAM,
    WASH_PROGRAM_FOR_DELICATES,
    SCHEDULED_STANDARD_WASH_PROGRAM,
    SCHEDULED_COLOR_WASH_PROGRAM,
    SCHEDULED_WASH_PROGRAM_FOR_DELICATES,
    CANCEL
}
