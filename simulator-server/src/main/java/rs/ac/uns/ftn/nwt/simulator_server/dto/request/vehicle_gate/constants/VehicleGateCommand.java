package rs.ac.uns.ftn.nwt.simulator_server.dto.request.vehicle_gate.constants;

public enum VehicleGateCommand {
    PUBLIC,
    PRIVATE,
    INACTIVE,
    CLOSED
}
