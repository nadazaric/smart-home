package rs.ac.uns.ftn.nwt.simulator_server.constants.washing_machine;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.NONE)
public  class WashingTime {
    public static int STANDARD_WASH_PROGRAM_IN_SECONDS = 1;
    public static int COLOR_WASH_PROGRAM_IN_SECONDS = 1;
    public static int WASH_PROGRAM_FOR_DELICATES_IN_SECONDS = 1;
}
