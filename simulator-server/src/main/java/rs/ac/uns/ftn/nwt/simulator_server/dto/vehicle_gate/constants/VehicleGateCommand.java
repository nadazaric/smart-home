package rs.ac.uns.ftn.nwt.simulator_server.dto.vehicle_gate.constants;

public enum VehicleGateCommand {
    IN,
    OUT,
    DENIED,
    NO_CHANGES,
    CLOSE,
    USER_CHANGE
}
