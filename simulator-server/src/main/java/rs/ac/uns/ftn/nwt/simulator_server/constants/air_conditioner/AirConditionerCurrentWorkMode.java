package rs.ac.uns.ftn.nwt.simulator_server.constants.air_conditioner;

public enum AirConditionerCurrentWorkMode {
    HEATING,
    COOLING,
    PERIODIC_COOLING,
    PERIODIC_HEATING,
    TEMPERATURE_MAINTENANCE,
    PERIODIC_TEMPERATURE_MAINTENANCE,
    OFF
}
