export interface VehicleGateInOutResponse {
  vehicleIn: boolean,
  timestamp: string
}
