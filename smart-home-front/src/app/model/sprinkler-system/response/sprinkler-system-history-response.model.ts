export interface SprinklerSystemHistoryResponse {
  status: string
  triggeredBy: string
  timestamp: string
}
