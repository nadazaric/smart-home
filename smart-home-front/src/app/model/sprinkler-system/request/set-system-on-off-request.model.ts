export interface SetSystemOnOffRequest {
  systemOn: boolean
  userEmail: string
}
