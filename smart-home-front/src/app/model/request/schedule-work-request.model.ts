export interface ScheduleWorkRequest {
  startTime : string
  endTime: string
  days: string[]
}
