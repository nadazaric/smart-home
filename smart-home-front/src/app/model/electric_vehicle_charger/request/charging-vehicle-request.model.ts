export interface ChargingVehicleRequest {
  currentPower: number,
  maxPower: number
}
