export interface ChargingVehicleResponse {
  id: number,
  currentPower: number,
  maxPower: number
}
