export interface FluxResultDto {
  value: any,
  timestamp: number,
}
