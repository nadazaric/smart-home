export interface WashingMachineHistoryResponse {
  executor: string;
  action: string;
  timestamp: string;
}